interface ICat {
  url: string;
}

export class Cat implements ICat {
  url: string;

  constructor(url: string) {
    this.url = url.includes("https") ? url : "this is not a valid url";
  }
}
