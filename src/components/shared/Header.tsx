import React from "react";
type Props = {
  setState: (val: boolean) => void;
};
const Header: React.FC<Props> = ({ setState }) => {
  const setHome = () => {
    console.log("set to home");
    setState(false);
    return;
  };

  const setToAPI = () => {
    console.log("set to API");
    setState(true);
    return;
  };
  return (
    <header className="border w-100 d-flex justify-content-between align-items-center">
      <h1>Learn to use API's</h1>
      <nav className="d-flex w-100 justify-content-around">
        <button onClick={() => setHome()}>Home</button>
        <button onClick={() => setToAPI()}>the API</button>
      </nav>
    </header>
  );
};

export default Header;
