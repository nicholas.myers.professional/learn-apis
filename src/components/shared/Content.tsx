import React, { useState, useEffect } from "react";

type Props = {
  state: boolean;
  cat: string;
};

export const Content: React.FC<Props> = ({ state, cat }) => {
  return (
    <main>
      {!state && <p>Home page content</p>}
      {state && (
        <p>
          API page content <img src={cat + ".jpg"} alt="this is a cat" />
        </p>
      )}
    </main>
  );
};
