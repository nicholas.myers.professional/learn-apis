import React, { useEffect, useState } from "react";
import { Content } from "./Content";
import Header from "./Header";
import axios from "axios";
import { Cat } from "../../dal/models/CatModel";

const App: React.FC = () => {
  const [isAPI, setIsAPI] = useState(false);

  const [cat, setCat] = useState("");

  const config = {
    headers: {
      "Access-Control-Allow-Origin": "https://http.cat/100",
    },
  };
  
  useEffect(() => {
    const catUrl = new Cat("https://http.cat/100");
    axios
      .get(catUrl.url)
      .then((res) => setCat(catUrl.url))
      .catch((err) => console.log(err));
    console.log(cat);
  }, []);
  return (
    <div className="App">
      <Header setState={setIsAPI} />
      <Content state={isAPI} cat={cat} />
    </div>
  );
};

export default App;
